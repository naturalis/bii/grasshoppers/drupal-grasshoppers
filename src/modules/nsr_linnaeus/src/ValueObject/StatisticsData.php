<?php

namespace Drupal\nsr_linnaeus\ValueObject;

use InvalidArgumentException;
use function array_diff_assoc;
use function array_keys;
use function is_array;
use function is_string;
use function str_replace;

final class StatisticsData {

  private const STATISTICS_KEYS = [
    'species_with_image' => '',
    'images' => '',
  ];

  /**
   * @var string
   */
  private string $mainCount;

  /**
   * @var string[][]
   */
  private array $statistics;

  /**
   * @param array $data
   */
  public function __construct(array $data) {
    $this->mainCount = $this->getString('all', $data);

    if (empty($data['statistics']) || !is_array($data['statistics'])) {
      throw new InvalidArgumentException("Missing 'statistics' key/data in statistics data");
    }
    $this->setStatistics($data['statistics']);
  }

  /**
   * @param string $key
   * @param array $data
   *
   * @return string
   *
   * @throws \InvalidArgumentException
   */
  private function getString(string $key, array $data): string {
    if (!isset($data[$key])) {
      throw new InvalidArgumentException("Missing '$key' in the statistics data");
    }
    if (!is_string($data[$key])) {
      throw new InvalidArgumentException("The '$key' is not a string in the statistics data");
    }
    return $data[$key];
  }

  /**
   * @param string[][] $statistics
   *
   * @throws \InvalidArgumentException
   */
  private function setStatistics(array $statistics): void {
    $diff = array_diff_key(self::STATISTICS_KEYS, $statistics);
    if ($diff !== []) {
      $subKeys = implode(', ', array_keys($diff));
      throw new InvalidArgumentException("Missing 'statistics' sub keys in the statistics data: $subKeys");
    }

    foreach (array_keys(self::STATISTICS_KEYS) as $key) {
      if (!is_array($statistics[$key])) {
        throw new InvalidArgumentException("The sub key '$key' is not an array in the statistics data");
      }
      try {
        $this->statistics[$key] = [
          'count' => $this->getString('count', $statistics[$key]),
          'label' => $this->getString('label', $statistics[$key]),
        ];
      }
      catch (InvalidArgumentException $exception) {
        $message = $exception->getMessage();
        $message = str_replace(
          [
            "'count'",
            "'label'",
          ],
          [
            "sub key 'count' from statistics '$key'",
            "sub key 'label' from statistics '$key'",
          ],
          $message
        );
        throw new InvalidArgumentException($message);
      }
    }
  }

  /**
   * @return string
   */
  public function getMainCount(): string {
    return $this->mainCount;
  }

  /**
   * @return string[][]
   */
  public function getStatistics(): array {
    return $this->statistics;
  }

  /**
   * @param \Drupal\nsr_linnaeus\ValueObject\StatisticsData $other
   *
   * @return bool
   */
  public function isEqual(StatisticsData $other): bool {
    if ($this->mainCount !== $other->getMainCount()) {
      return FALSE;
    }
    $otherStatistics = $other->getStatistics();
    foreach ($this->statistics as $key => $statistic) {
      if (array_diff_assoc($statistic, $otherStatistics[$key])) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
