<?php

namespace Drupal\nsr_linnaeus\Controller;

use Drupal\Core\Controller\ControllerBase;

final class LinnaeusTemplateController extends ControllerBase {

  /**
   * @return string[]
   */
  public function page(): array {
    return [
      '#markup' => '[linnaeus template content]',
    ];

  }

}
