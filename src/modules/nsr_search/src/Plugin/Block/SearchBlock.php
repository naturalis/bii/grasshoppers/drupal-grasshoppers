<?php

namespace Drupal\nsr_search\Plugin\Block;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *   id = "search_block",
 *   admin_label = @Translation("Search block"),
 * )
 */
class SearchBlock extends BlockBase {

  /**
   * @return string[]|array[]
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function build(): array {
    return [
      '#attributes' => [
        'class' => ['search-block'],
      ],
      'form' => [
        '#type' => 'form',
        '#method' => 'get',
        '#action' => '/linnaeus_ng/rewrite.php',
        '#attributes' => [
          'class' => ['search-form'],
        ],
        'form_id' => [
          '#type' => 'hidden',
          '#name' => 'form_id',
          '#value' => 'search_block_form',
        ],
        'search' => [
          '#type' => 'textfield',
          '#id' => 'search-' . Crypt::randomBytesBase64(8),
          '#name' => 'search_block_form',
          '#title' => $this->t('Search'),
          '#title_display' => 'invisible',
          '#placeholder' => 'Search for a species/taxon...',
          '#attributes' => [
            'class' => ['search'],
          ],
        ],
        'submit' => [
          '#type' => 'submit',
          '#value' => 'More search options',
          '#attributes' => [
            'class' => ['search-button'],
          ],
        ],
      ],
    ];
  }

}
