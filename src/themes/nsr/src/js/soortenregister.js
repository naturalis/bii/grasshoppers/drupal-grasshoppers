
(function ($) {
  class Soortenregister {
    constructor() {
      this.minInputLength = 3;
      this.searchUrl = "/linnaeus_ng/app/views/search/nsr_ajax_interface.php";
      this.searchWait = 300;
      this.resultUrl = "/linnaeus_ng/app/views/species/nsr_taxon.php?id=";
      this.lineTpl = '<li id="item-%IDX%" ident="%IDENT%" onclick="return setSuggestionId(this);"><div class="common">%COMMON_NAME%</div><div class="scientific">%SCIENTIFIC_NAME%</div></li>';
      this.escKey = 27;
      this.enterKey = 13;
      this.search = '';
      this.listdata = [];
      this.prevSearch = '';

      this.debouncedFindSuggestions = this.debounce(this.findSuggestions.bind(this), this.searchWait);
      this.setupSuggestionSearch();
    }

    debounce(func, wait) {
      let timeout;
      return function(...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(this, args), wait);
      };
    }

    stripTags(raw) {
      if (raw) {
        return raw.replace(/(<([^>]+)>)/ig, "");
      }
    }

    getTimeStamp() {
      const now = new Date();
      return now.getTime();
    }

    retrieveSuggestions(searchValue) {
      if (searchValue.length < this.minInputLength) {
        return;
      }

      $.ajax({
        url: this.searchUrl,
        type: "POST",
        data: {
          action: this.searchFieldId + '_suggestions',
          search: searchValue,
          time: this.getTimeStamp()
        },
        success: (data) => {
          if (!data) {
            return;
          }
          this.setListData($.parseJSON(data));
          this.buildSuggestions();
          this.revealSuggestions();
        }
      });
    }

    setListData(data) {
      this.listdata = data;
    }

    hideSuggestions(ele) {
      if (ele) {
        $(ele).hide();
      } else {
        $('div[id*=suggestion]').hide();
      }
    }

    revealSuggestions() {
      if (this.listdata && this.listdata.length > 0) {
        $('#' + this.searchFieldId + '_suggestion').show();
      }
    }

    redirectToSuggestion(ele) {
      const id = $(ele).attr("ident");
      const url = this.resultUrl + id;
      window.location.href = window.location.origin + url;
      return false;
    }

    buildSuggestions() {
      let suggestions = [];
      for (let i in this.listdata) {
        let l = this.listdata[i];
        suggestions.push(
          this.lineTpl
            .replace('%IDX%', i)
            .replace(/%IDENT%/g, (l.id ? l.id : ''))
            .replace(/%LABEL%/g, this.stripTags(l.label))
            .replace(/%SCIENTIFIC_NAME%/g, this.stripTags(l.scientific_name ? l.scientific_name : ''))
            .replace(/%SCIENTIFIC_NAME_DATA%/g, this.stripTags(l.scientific_name ? l.scientific_name : l.label))
            .replace(/%COMMON_NAME%/g, this.stripTags(l.common_name ? l.common_name : (l.nomen ? l.nomen : l.scientific_name)))
        );
      }
      $('#' + this.searchFieldId + '_suggestion').html('<ul>' + suggestions.join('') + '</ul>');
    }

    findSuggestions() {
      this.search = $('#' + this.searchFieldId).val();
      if (this.search !== this.prevSearch) {
        this.hideSuggestions();
        this.retrieveSuggestions(this.search);
        this.prevSearch = this.search;
      }
    }

    setupSuggestionSearch() {
      this.searchFieldId = $('div.suggestion-list').attr('id').replace('_suggestion', '');
      $('#' + this.searchFieldId).keyup((event) => {
        if (event.keyCode === this.escKey) {
          this.hideSuggestions();
          return;
        }
        if (event.keyCode === this.enterKey) {
          let suggestions = $('#' + this.searchFieldId + '_suggestion ul li');
          if (suggestions.length > 0) {
            let suggestion = $('#' + this.searchFieldId + '_suggestion ul li.selected');
            if (suggestion.length === 0) {
              suggestion = suggestions.first();
            }
            if (suggestion) {
              this.redirectToSuggestion(suggestion);
              event.preventDefault();
            }
          }
          return;
        }
        this.debouncedFindSuggestions();
        event.preventDefault();
        return false;
      });
    }
  }

  // only do this if the url dos not contain 'linnaeus_ng'
  if (window.location.href.indexOf('linnaeus_ng') === -1) {
    let searchRegister = new Soortenregister();
    window.setSuggestionId = function(element) {
      searchRegister.redirectToSuggestion(element);
    }
  }
})(jQuery);
