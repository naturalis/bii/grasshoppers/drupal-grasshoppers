const Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

const encore = Encore
  .setOutputPath('dist/')
  .setPublicPath('/themes/custom/nsr/dist/')
  .setManifestKeyPrefix('')
  .addEntry('script', ['./src/js/script.js', './src/js/soortenregister.js'])
  .addStyleEntry('style', './src/sass/style.sass')
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())
  .enableSassLoader()
;

module.exports = encore.getWebpackConfig();
